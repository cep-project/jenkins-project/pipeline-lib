def call(){
    env.PROJECT_REPO = "https://gitlab.com/cep-project/jenkins-project/jenkins-pipeline.git"

    env.REGISTRY_URL = "docker.io"
    env.DOCKER_USER = "darkhero101"
    env.DOCKER_PASSWORD = "sT2gtLyVHPujyt-Kttiu"

    env.APP_IMAGE = "hello-app"
    env.APP_TAG = "latest"
    env.LB_IMAGE = "hello-lb"
    env.LB_TAG = "latest"

    env.ANSIBLE_REPO = "https://gitlab.com/cep-project/ansible-project.git"
    env.TERRAFORM_REPO = "https://gitlab.com/cep-project/terraform-project.git"
}
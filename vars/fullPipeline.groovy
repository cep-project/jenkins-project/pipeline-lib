def call(){
    pipelineEnv()

    node ("linux-ubuntu20") {
        stage('Clone project') {
            repoCheckout(
                [
                    branch: "main",
                    repo_url: "${env.PROJECT_REPO}"
                ]
            )
        }
        stage('Build') {
            buildJob()
            pushJob()
        }
        stage('Provision') {
            repoCheckout(
                [
                    branch: "main",
                    repo_url: "${env.TERRAFORM_REPO}"
                ]
            )
            tfProvision()
        }
        stage('Deploy') {
            repoCheckout(
                [
                    branch: "main",
                    repo_url: "${env.ANSIBLE_REPO}"
                ]
            )
            ansibleDeploy()
        }
    }
}